<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<%@ page import="java.util.List"%>
<%@ page import="com.agileai.domain.DataRow"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
List<DataRow> oppRecords = (List<DataRow>)request.getAttribute("oppRecords");
List<DataRow> orderRecords = (List<DataRow>)request.getAttribute("orderRecords");
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>工作总结</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript" src="aeditors/xheditor/xheditor-1.2.2.min.js"></script>
<script type="text/javascript" src="aeditors/xheditor/xheditor_lang/zh-cn.js"></script>
<script language="javascript">
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%if ("SubmitPlan".equals(pageBean.selectedValue("TASK_REVIEW_STATE"))){%>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'confirmPlan'})"><input value="&nbsp;" type="button" class="confirmImgBtn" title="确认计划" />确认计划</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'repulsePlan'})"><input value="&nbsp;" type="button" class="removeImgBtn" title="打回计划" />打回计划</td>
<%} %>
<%if ("SubmitSummary".equals(pageBean.selectedValue("TASK_REVIEW_STATE"))){%>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'confirmSummary'})"><input value="&nbsp;" type="button" class="confirmImgBtn" title="确认总结" />确认总结</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'repulseSummary'})"><input value="&nbsp;" type="button" class="removeImgBtn" title="打回总结" />打回总结</td>
<%} %>
</tr>
</table>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr >
	<th width="100" colspan="3" nowrap>起止时间</th>
	<td><input id="TC_BEGIN" label="起始时间" name="TC_BEGIN" type="text" readonly="readonly" value="<%=pageBean.inputValue("TC_BEGIN")%>" size="10" class="text" />
		至
    	<input id="TC_END" label="结束时间" name="TC_END" type="text" readonly="readonly" value="<%=pageBean.inputValue("TC_END")%>" size="10" class="text" />
</td>
	<th width="100" nowrap>当前状态</th>
	<td><input id="TASK_REVIEW_STATE" label="当前状态" name="TASK_REVIEW_STATE" type="text" value="<%=pageBean.selectedText("TASK_REVIEW_STATE")== null?"没有计划":pageBean.selectedText("TASK_REVIEW_STATE")%>" size="25" class="text" readonly="readonly"/>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td colspan="2"><label>本周新增<strong style="color: red;"><%=pageBean.inputValue("PRO_NUM")%></strong>条潜条在客户,新增<strong style="color: red;"><%=pageBean.inputValue("CUST_NUM")%></strong>条客户记录。</label></td>
</tr>
<tr>
	<td colspan="2"><label>本周陌生拜访共<strong style="color: red;"><%=pageBean.inputValue("TASK_REVIEW_STRANGE_TASK")%></strong>条任务记录,跟进<strong style="color: red;"><%=pageBean.inputValue("TASK_REVIEW_STRANGE_FOLLOW")%></strong>条,拜访记录<strong style="color: red;"><%=pageBean.inputValue("TASK_REVIEW_STRANGE_VISIT")%></strong>条,实际记录<strong style="color: red;"><%=pageBean.inputValue("TASK_REVIEW_STRANGE_ACTUAL")%></strong>条</label></td>
</tr>
<tr>
	<td colspan="2"><label>本周意向跟进共<strong style="color: red;"><%=pageBean.inputValue("TASK_REVIEW_INTENTION_TASK")%></strong>条任务记录,跟进<strong style="color: red;"><%=pageBean.inputValue("TASK_REVIEW_INTENTION_FOLLOW")%></strong>条,拜访记录<strong style="color: red;"><%=pageBean.inputValue("TASK_REVIEW_INTENTION_VISIT")%></strong>条,实际记录<strong style="color: red;"><%=pageBean.inputValue("TASK_REVIEW_INTENTION_ACTUAL")%></strong>条</label></td>
</tr>
<tr>
	<td colspan="2"><label>商机数<strong style="color: red;"><%=pageBean.inputValue("OPP_NUM")%></strong>条,订单数<strong style="color: red;"><%=pageBean.inputValue("ORDER_NUM")%></strong>条</label></td>
</tr>
</table>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th>商机名称</th>
	<th>客户名称</th>
	<th>联系人名称</th>
	<th>关注产品</th>
	<th>跟进人员</th>
	<th>创建人</th>
	<th>创建时间</th>
</tr>
<%
	for(int i=0 ;i<oppRecords.size();i++){
		DataRow oppRecord = oppRecords.get(i);
%>
<tr>
	<td><%=oppRecord.get("OPP_NAME") == null?"":oppRecord.get("OPP_NAME")%></td>
	<td><%=oppRecord.get("CUST_ID_NAME") == null?"":oppRecord.get("CUST_ID_NAME")%></td>
	<td><%=oppRecord.get("CONT_ID_NAME") == null?"":oppRecord.get("CONT_ID_NAME")%></td>
	<td><%=oppRecord.get("OPP_CONCERN_PRODUCT") == null?"":oppRecord.get("OPP_CONCERN_PRODUCT")%></td>
	<td><%=oppRecord.get("CLUE_SALESMAN_NAME") == null?"":oppRecord.get("CLUE_SALESMAN_NAME")%></td>
	<td><%=oppRecord.get("OPP_CREATER_NAME") == null?"":oppRecord.get("OPP_CREATER_NAME")%></td>
	<td><%=oppRecord.get("OPP_CREATE_TIME") == null?"":oppRecord.get("OPP_CREATE_TIME")%></td>
</tr>
<%} %>
</table>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th>订单名称</th>
	<th>客户名称</th>
	<th>负责人</th>
	<th>订单费用</th>
	<th>跟进人员</th>
	<th>创建人</th>
	<th>创建时间</th>
</tr>
<%
	for(int i=0 ;i<orderRecords.size();i++){
		DataRow orderRecord = orderRecords.get(i);
%>
<tr>
	<td><%=orderRecord.get("OPP_ID_NAME") == null?"":orderRecord.get("OPP_ID_NAME")%></td>
	<td><%=orderRecord.get("ORDER_NAME") == null?"":orderRecord.get("ORDER_NAME")%></td>
	<td><%=orderRecord.get("ORDER_CHIEF")== null?"":orderRecord.get("ORDER_CHIEF")%></td>
	<td><%=orderRecord.get("ORDER_COST") == null?"0.00":orderRecord.get("ORDER_COST")%></td>
	<td><%=orderRecord.get("CLUE_SALESMAN_NAME") == null?"":orderRecord.get("CLUE_SALESMAN_NAME")%></td>
	<td><%=orderRecord.get("ORDER_CREATER_NAME") == null?"":orderRecord.get("ORDER_CREATER_NAME")%></td>
	<td><%=orderRecord.get("ORDER_CREATE_TIME") == null?"":orderRecord.get("ORDER_CREATE_TIME")%></td>
</tr>
<%} %>
</table>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
<td class="header" style="background-color: rgb(217, 233, 255);">
	<span style="line-height: 30px;font-weight: bolder;color: #036;border-top: 0px;border-left: 0px;padding: 1px 1px 1px 10px;background: #D9E9Ff;text-align: left;
    margin: 0px;">工作总结</span>
</td>
</tr>
<tr>
	<td colspan="2">
		<textarea style="width:100%;" id="TASK_REVIEW_DESC" name="TASK_REVIEW_DESC" cols="60" rows="10" 
		class="xheditor {skin:'o2007blue',tools:'Bold,Italic,Underline,Strikethrough,FontColor,BackColor,SelectAll,Removeformat,List,Outdent,Indent,Link,Unlink,Fullscreen'}">
		<%=pageBean.inputValue("TASK_REVIEW_DESC")%></textarea>
	</td>
</tr>
</table>
</div>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="TASK_REVIEW_ID" id="TASK_REVIEW_ID" value="<%=pageBean.inputValue("TASK_REVIEW_ID")%>"/>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
