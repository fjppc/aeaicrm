package com.agileai.crm.module.procustomer.service;

import com.agileai.crm.module.procustomer.service.UserListSelect;
import com.agileai.hotweb.bizmoduler.core.PickFillModelServiceImpl;

public class UserListSelectImpl
        extends PickFillModelServiceImpl
        implements UserListSelect {
    public UserListSelectImpl() {
        super();
    }
}
