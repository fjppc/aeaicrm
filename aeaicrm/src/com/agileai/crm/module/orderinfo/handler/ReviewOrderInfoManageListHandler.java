package com.agileai.crm.module.orderinfo.handler;

import java.util.List;

import com.agileai.crm.common.PrivilegeHelper;
import com.agileai.crm.cxmodule.OrderInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.MasterSubListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class ReviewOrderInfoManageListHandler extends MasterSubListHandler {
	public ReviewOrderInfoManageListHandler() {
		super();
		this.editHandlerClazz = ReviewOrderInfoManageEditHandler.class;
		this.serviceId = buildServiceId(OrderInfoManage.class);
	}
	public ViewRenderer prepareDisplay(DataParam param){
		User user = (User) this.getUser();
		PrivilegeHelper privilegeHelper = new PrivilegeHelper(user);
		if(privilegeHelper.isSalesDirector()){
			setAttribute("doConfirm", true);
		}else if(!privilegeHelper.isSalesDirector()){
			param.put("CLUE_SALESMAN", user.getUserId());
			param.put("ORDER_CREATER", user.getUserId());
			setAttribute("doConfirm", false);
		}
		
		
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().findMasterRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	protected void processPageAttributes(DataParam param) {
		setAttribute("ORDER_STATE", FormSelectFactory.create("ORDER_STATE")
				.addSelectedValue(param.get("ORDER_STATE")));
		setAttribute("ORDER_CHIEF", FormSelectFactory.create("ORDER_CHIEF")
				.addSelectedValue(param.get("ORDER_CHIEF")));
		initMappingItem("ORDER_STATE", FormSelectFactory.create("ORDER_STATE")
				.getContent());
	}

	protected void initParameters(DataParam param) {
		initParamItem(param, "ORDER_STATE", "");
		initParamItem(param, "OPP_NAME", "");
		initParamItem(param, "ORDER_CHIEF", "");
	}

	@PageAction
	public ViewRenderer doConfirm(DataParam param) {
		storeParam(param);
		return new DispatchRenderer(getHandlerURL(this.editHandlerClazz) + "&"
				+ OperaType.KEY + "=doConfirm&comeFrome=doConfirm");
	}

	protected OrderInfoManage getService() {
		return (OrderInfoManage) this.lookupService(this.getServiceId());
	}
}
