package com.agileai.crm.module.customer.handler;

import java.util.List;

import com.agileai.crm.module.customer.service.CustomerSelect;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.PickFillModelHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class CustomerSelectListHandler
        extends PickFillModelHandler {
    public CustomerSelectListHandler() {
        super();
        this.serviceId = buildServiceId(CustomerSelect.class);
    }
    
	public ViewRenderer prepareDisplay(DataParam param){
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().queryPickFillRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

    protected void processPageAttributes(DataParam param) {
    	this.setAttribute("custName", param.get("custName"));
        initMappingItem("CUST_INDUSTRY",
                FormSelectFactory.create("CUST_INDUSTRY")
                                 .getContent());
    }

    protected void initParameters(DataParam param) {
    }
    
    protected CustomerSelect getService() {
        return (CustomerSelect) this.lookupService(this.getServiceId());
    }
}
